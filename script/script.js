const url = `https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=UWuvuzd1P0Jcmf9Ju1KREc5Vq5pga4sRIsuatkSp`;

const image = document.querySelector('.picofday');
const caption = document.querySelector('.pic');

fetch(url)
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    image.src = data.photos[0].img_src;
    caption.innerHTML = data.photos[0].rover.name;
  });

